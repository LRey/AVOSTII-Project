// ----------------------------------------- Shot Fire par Quentin Fombaron ----------------------------------------- //

window.onload = function() {

// ----------------------------------------- Définition du Canvas --------------------------------------------------- //

    var monCanvas = document.getElementById('mon_canvas');
    if (!monCanvas) {
        alert("Impossible de récupérer le canvas");
        return;
    }

    var ctx = monCanvas.getContext("2d");
    if (!ctx) {
        alert("Impossible de récupérer le ctx du canvas");
        return;
    }

// ----------------------------------------- Définition des Fonctions ----------------------------------------------- //

    function resize() {
        monCanvas = document.getElementById('mon_canvas');
        monCanvas.width = window.innerWidth;
        monCanvas.height = window.innerHeight;
        //console.log(monCanvas.width+";"+monCanvas.height);
    }

    function timer() {
        duree--;
    }

    function display() {
        if (duree != 0) {
            ctx.clearRect(0, 0, monCanvas.width, monCanvas.height);
        }

        ctx.fillStyle = noir;
        ctx.font = (0.035*wHeight)+"px Minecraft";
        ctx.fillText("Score : " + score, wWidth/20, wHeight/11); // Score
        ctx.font = (0.05*wHeight)+"px Minecraft";
        ctx.fillText(duree, wWidth/2, wHeight/11);

        for (var i = 0; i < coordCib.length; i++) {
            if ((coordCib[i][2] == 1) && (coordCib[i][3][0] == cible_1)) {
                ctx.drawImage(coordCib[i][3][0], coordCib[i][0], coordCib[i][1], 0.1*wWidth, 0.07*wHeight);
            }
            else if ((coordCib[i][2] == 1) && (coordCib[i][3][0] == cible_2)) {
                ctx.drawImage(coordCib[i][3][0], coordCib[i][0], coordCib[i][1], 0.082*wWidth, 0.072*wHeight);
            }
            else if (coordCib[i][2] == 1) {
                ctx.drawImage(coordCib[i][3][0], coordCib[i][0], coordCib[i][1], 0.073*wWidth, 0.073*wHeight);
            }
        }

        makeTarget();

        ctx.drawImage(pauseImg, wWidth-((wWidth/6)+(0.01*wHeight)), 0.03*wHeight, wWidth/6, wWidth/6);

        if (headshotChk){
            ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
            ctx.fillRect(0, 0, wWidth, wHeight);
            ctx.drawImage(headshotImg, wWidth/8, (wHeight/2)-(3*wWidth/8), (3*wWidth)/4, (3*wWidth)/4);
        }

        if (duree == 0) {
            clearInterval(displayStop);
            clearInterval(timerStop);
            clearInterval(randomChoice);
            gameover = true;
            finish();
        }
    }

    function random() {
        for (var i=0; i < 4; i++) {
            var x = Math.floor(Math.random() * coordCib.length);
            coordCib[x][2] = true;
        }

        setTimeout(function () {
            for (var i=0; i < coordCib.length; i++) {
                coordCib[i][2] = false;}
            }, 10000);
    }

    function makeTarget() {
        ctx.drawImage(target, targetX, targetY, targetWidth, targetHeight);
    }

    function clicCanvas(e) {
        var xSourisDocument = e.pageX;
        var ySourisDocument = e.pageY;

        var xCanvas = monCanvas.offsetLeft;
        var yCanvas = monCanvas.offsetTop;

        var xSourisCanvas = xSourisDocument - xCanvas;
        var ySourisCanvas = ySourisDocument - yCanvas;

        if (preloadChk) {
            load();
        }

        for (var i = 0; i < coordCib.length; i++) {

            var OpeX = (targetX + (targetWidth/2)) - coordCib[i][0];
            var OpeY = (targetY+ (targetHeight/2)) - coordCib[i][1];

            if ( ( ( ( OpeX > 0 ) && ( OpeX < coordCib[i][3][1] ) )
                &&  ( ( OpeY > coordCib[i][3][2]/2 ) && ( OpeY < coordCib[i][3][2] ) ) )
                && (coordCib[i][2]) ) {
                coordCib[i][2] = false;
                score += 100;
                duree += 3;
                setTimeout(function () {
                    kill.play();
                }, 500);

                shotNbr++;
                bodyshotChk = true;
                setTimeout(function () {
                    bodyshotChk = false;
                }, 2000);

                break;
            }

            else if ( ( ( ( OpeX > coordCib[i][3][1]/4 ) && ( OpeX < ((3*coordCib[i][3][1])/4) ) )  //HEADSHOT
                &&  ( ( OpeY > 0 ) && ( OpeY < coordCib[i][3][2]/2 ) ) )
                && (coordCib[i][2]) ) {
                coordCib[i][2] = false;
                score += 200;
                duree += 5;

                setTimeout(function () {
                 headshotSnd.load();
                 headshotSnd.play();
                }, 500);

                setTimeout(function () {
                    headshotChk = false;
                }, 1000);

                shotNbr++;
                headshotNbr++;
                headshotChk = true;

                break;
            }

        }

        if  (( (( xSourisCanvas - (wWidth-((wWidth/6)+(0.01*wHeight))) > 0) && (xSourisCanvas - (wWidth-((wWidth/6)+(0.01*wHeight))) < wWidth/6))
            && (( ySourisCanvas - (0.03*wHeight)  > 0) && (ySourisCanvas - (0.03*wHeight) < wWidth/6)))
            && (!gameover)){

            if (!pause){
                pause = true;
                finish();
                clearInterval(displayStop);
                clearInterval(timerStop);
                clearInterval(randomChoice);
            }

            else if (pause) {
                setTimeout(function () {
                    pause = false;
                }, 100);
                var checkTrue = false;
                for (var j=0; j < coordCib.length; j++) {
                    if (coordCib[j][2] == true) {
                        checkTrue = true;
                    }
                }
                if (!checkTrue){
                    random();
                }
                timerStop = setInterval(timer, 1000);
                randomChoice = setInterval(random, 12000);
                displayStop = setInterval(display, 50);
            }
        }

        if ((duree != 0) && (!pause) && (!preloadChk)) {
            fire.load();
            fire.play();
            setTimeout(function () {
                reload.play();
            }, 700);
            fireNbr++;
        }

        if  (( (( xSourisCanvas - (0.1*wWidth) > 0) && (xSourisCanvas - (0.1*wWidth) < wWidth-(0.2*wWidth)))
            && (( ySourisCanvas - ((3*wHeight)/4)  > 0) && (ySourisCanvas - ((3*wHeight)/4) < wHeight/8)))
            && (gameover || pause)) {
                load();
        }
    }

    function orientation(event) {
            if ( (targetX <= (wWidth - targetWidth)) && (targetX >= 0) ) {
                targetX += event.gamma/2;

                if (targetX > wWidth - targetWidth) {
                    targetX = wWidth - targetWidth;
                }

                else if (targetX < 0) {
                    targetX = 0;
                }

            }

            if ( (targetY <= (wHeight - targetHeight)) && (targetY >= 0) ) {
                targetY += event.beta/2;

                if (targetY > wHeight - targetHeight) {
                    targetY = wHeight - targetHeight;
                }

                else if (targetY < 0) {
                    targetY = 0;
                }
            }

    }

    function finish() {
        /*ctx.clearRect(0, 0, wWidth, wHeight/8);
        ctx.fillStyle = "rgba(0, 0, 0, 0.7)";
        ctx.fillRect(0, 0, wWidth, wHeight);
        ctx.shadowBlur = 100;
        ctx.shadowColor = noir;*/
        ctx.drawImage(backgroundBlurred, 0, 0, wWidth, wHeight);

        if (pause) {
            ctx.drawImage(playImg, wWidth-((wWidth/6)+(0.01*wHeight)), 0.03*wHeight, wWidth/6, wWidth/6);
        }

        ctx.fillStyle = "rgb(23, 145, 167)";
        ctx.beginPath();
        ctx.moveTo(0.1*wWidth, (wHeight/4)+(0.05*wWidth));
        ctx.quadraticCurveTo(0.1*wWidth, wHeight/4, 0.14*wWidth, wHeight/4);
        ctx.lineTo(wWidth-(0.14*wWidth), wHeight/4);
        ctx.quadraticCurveTo(wWidth-(0.1*wWidth), wHeight/4, wWidth-(0.1*wWidth), (wHeight/4)+(0.05*wWidth));
        ctx.lineTo(wWidth-(0.1*wWidth), ((5*wHeight)/8)-50);
        ctx.quadraticCurveTo(wWidth-(0.1*wWidth), (5*wHeight)/8, wWidth-(0.14*wWidth), (5*wHeight)/8);
        ctx.lineTo(0.14*wWidth, (5*wHeight)/8);
        ctx.quadraticCurveTo(0.1*wWidth, (5*wHeight)/8, 0.1*wWidth, ((5*wHeight)/8)-(0.05*wWidth));
        ctx.closePath();
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(0.1*wWidth, ((3*wHeight)/4)+(0.05*wWidth));
        ctx.quadraticCurveTo(0.1*wWidth, (3*wHeight)/4, 0.14*wWidth, (3*wHeight)/4);
        ctx.lineTo(wWidth-(0.14*wWidth), (3*wHeight)/4);
        ctx.quadraticCurveTo(wWidth-(0.1*wWidth), (3*wHeight)/4, wWidth-(0.1*wWidth), ((3*wHeight)/4)+50);
        ctx.lineTo(wWidth-(0.1*wWidth), ((7*wHeight)/8)-(0.05*wWidth));
        ctx.quadraticCurveTo(wWidth-(0.1*wWidth), (7*wHeight)/8, wWidth-(0.14*wWidth), (7*wHeight)/8);
        ctx.lineTo(0.14*wWidth, (7*wHeight)/8);
        ctx.quadraticCurveTo(0.1*wWidth, (7*wHeight)/8, 0.1*wWidth, ((7*wHeight)/8)-(0.05*wWidth));
        ctx.closePath();
        ctx.fill();

        ctx.font = (0.06*wHeight)+"px Minecraft";
        ctx.fillStyle = "white";
        ctx.fillText("SCORE : "+score, wWidth/7, wHeight/4);

        ctx.font = (0.04*wHeight)+"px Minecraft";
        ctx.fillText("Headshot : "+headshotNbr, wWidth/7, (7*wHeight)/20);

        if (fireNbr != 0) {
            ratio = (shotNbr/fireNbr)*100;
        }

        var ratioStg = ratio.toString();
        ctx.fillText("Precision : "+ratioStg.slice(0, 4)+" %", wWidth/7, (9*wHeight)/20);

        if (userdata.score < score) {
            userdata.score = JSON.stringify(score);
        }
        console.log(localStorage["HighScoresShot"]);

        localstore.push(userdata);
        localStorage["HighScoresShot"] = JSON.stringify(localstore);

        ctx.fillText("Meilleur score = "+userdata.score, wWidth/7, (11*wHeight)/20);

        ctx.font = (0.055*wHeight)+"px Minecraft";
        ctx.drawImage(replayImg, wWidth/6, 0.78*wHeight, wWidth/8, wWidth/8);
        ctx.fillText("REJOUER", (0.35*wWidth), (16.7*wHeight)/20);



    }

    function load() {

        for (var i=0; i < coordCib.length; i++) {
            coordCib[i][2] = false;
        }

        duree = 20;// En secondes
        score = 0;
        headshotChk = false;
        bodyshotChk = false;
        headshotNbr = 0;
        shotNbr = 0;
        fireNbr = 0;
        ratio = 0;
        gameover = false;
        pause = false;
        setInterval(function () {
            preloadChk = false;
        }, 100);

        targetX = (wWidth / 2) - (targetWidth / 2);
        targetY = (wHeight / 2) - (targetHeight / 2);

        sound.play();
        setInterval(function () {
            sound.currentTime = 0;
            sound.play();
        }, 60000);

        random();
        timerStop = setInterval(timer, 1000);
        randomChoice = setInterval(random, 12000);
        displayStop = setInterval(display, 16);
    }

    function preLoad() {
        resize();
        score = 0;
        //localStorage.clear();
        if (localStorage["HighScoresShot"] == undefined) {
            localStorage["HighScoresShot"] = [];
            localstore = [];
        }
        else {
            console.log(localStorage["HighScoresShot"]);
            localstore = JSON.parse(localStorage["HighScoresShot"]);
        }

        setTimeout(function () {

            if (typeof localstore == typeof []) {

                for (var i = 0; i < localstore.length; i++) {
                    if (localstore[i].user == username) {
                        find = true;
                        userdata = localstore[i];
                        localstore.splice(i, 1);
                    }

                }
                if (find) {
                    if (userdata.score < score) {
                        userdata.score = JSON.stringify(score);
                    }
                }

                else if (!find) {
                    userdata = {user: username, score: JSON.stringify(score)};
                }
            }
            else {
                localstore = [];
                userdata = {user: username, score: JSON.stringify(score)};
            }
            localstore.push(userdata);
            localStorage["HighScoresShot"] = JSON.stringify(localstore);
            preloadChk = true;

            ctx.drawImage(backgroundBlurred, 0, 0, wWidth, wHeight);
            ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
            ctx.fillRect(0, 0, wWidth, wHeight);
            ctx.font = (0.055*wHeight)+"px Avenir";
            ctx.fillStyle = "white";
            ctx.fillText("TAP POUR JOUER", 0.1*wWidth, wHeight/2);

            ctx.drawImage(trophy, 0.11*wWidth, 0.56*wHeight, wWidth/11, wWidth/11);
            ctx.drawImage(user, 0.15*wWidth, 0.33*wHeight, wWidth/10, wWidth/10);
            ctx.font = (0.034*wHeight)+"px Avenir";
            ctx.fillText("Bonjour " + username+" !", 0.3*wWidth, 0.38*wHeight);
            ctx.fillText("Ton Meilleur Score : " + userdata.score, 0.23*wWidth, (6*wHeight)/10);

            var x = 0;
            var y;
            for (var j = 0; j < localstore.length; j++) {
                if (localstore[j].score > x) {
                    x = localstore[j].score;
                    y = localstore[j].user;
                }
            }

            if (username != y && y != undefined) {
                ctx.fillText("Meilleur Score de "+ y +" : "+ x, 0.05*wWidth, (8*wHeight)/10);
            }
            else if (username == y && y != undefined){
                ctx.fillText("Personne n'a encore reussi", 0.13*wWidth, (8*wHeight)/10);
                ctx.fillText("a te battre !", 0.35*wWidth, (8.5*wHeight)/10);
            }

        }, 1000);
    }

    /*function getMousePos(canvas, evt) {
     var rect = canvas.getBoundingClientRect();
     return {
     x: evt.clientX - rect.left,
     y: evt.clientY - rect.top
     };
     }*/

// ----------------------------------------- Définition des Variables ----------------------------------------------- //


    // --- VARIABLES --- //
    var score;
    var duree;
    var headshotChk;
    var headshotNbr;
    var bodyshotChk;
    var shotNbr;
    var fireNbr;
    var ratio;
    var pause;
    var gameover;
    var preloadChk;

    var Url = {
        get get() {
            var vars = {};
            if (window.location.search.length !== 0)
                window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                    key = decodeURIComponent(key);
                    if (typeof vars[key] === "undefined") {
                        vars[key] = decodeURIComponent(value);
                    }
                    else {
                        vars[key] = [].concat(vars[key], decodeURIComponent(value));
                    }
                });
            return vars;
        }
    };
    var username = Url.get.pseudo;
    var find = false;
    var userdata = {};
    var localstore;

    var wWidth = window.innerWidth;
    var wHeight = window.innerHeight;
    var targetWidth = wWidth / 5;
    var targetHeight = wWidth / 5;
    var targetX = (wWidth / 2) - (targetWidth / 2);
    var targetY = (wHeight / 2) - (targetHeight / 2);

    var noir = "#000000";

    // --- IMAGES --- //
    var cible_1 = new Image();
    cible_1.src = "images/target_1.png";
    var cible_2 = new Image();
    cible_2.src = "images/target_2.png";
    var cible_3 = new Image();
    cible_3.src = "images/target_3.png";
    var cible_4 = new Image();
    cible_4.src = "images/target_4.png";
    var target = new Image();
    target.src = "images/viseur.png";
    var headshotImg = new Image();
    headshotImg.src = "images/headshot.png";
    var pauseImg = new Image();
    pauseImg.src = "images/pause.png";
    var replayImg = new Image();
    replayImg.src = "images/replay.png";
    var playImg = new Image();
    playImg.src = "images/play.png";
    var backgroundBlurred = new Image();
    backgroundBlurred.src = "images/iphone_background_blurred.jpg";
    var trophy = new Image();
    trophy.src = "images/trophy.png";
    var user = new Image();
    user.src = "images/user.png";

    // --- AUDIO --- //
    var fire = new Audio('audio/sniper.mp3');
    var headshotSnd = new Audio('audio/headshot.mp3');
    var kill = new Audio('audio/kill.mp3');
    kill.volume = 0.5;
    var sound = new Audio('audio/background.mp3');
    sound.volume = 0.15;
    var reload = new Audio('audio/reload.mp3');
    reload.volume = 0.3;

    // --- DICTONNAIRES COORDONNEE / TAILLE --- //
    cible1 = [cible_1, 0.1*wWidth, 0.07*wHeight];
    cible2 = [cible_2, 0.082*wWidth, 0.072*wHeight];
    cible3 = [cible_3, 0.073*wWidth, 0.073*wHeight];
    cible4 = [cible_4, 0.073*wWidth, 0.073*wHeight]; // [image, width, height]


    coordCib = [[0.144*wWidth, 0.476*wHeight, false, cible1],
        [0.347*wWidth, 0.476*wHeight, false, cible1],
        [0.550*wWidth, 0.476*wHeight, false, cible1],
        [0.752*wWidth, 0.476*wHeight, false, cible1],
        [0.144*wWidth, 0.245*wHeight, false, cible1],
        [0.347*wWidth, 0.245*wHeight, false, cible1],
        [0.550*wWidth, 0.245*wHeight, false, cible1],
        [0.752*wWidth, 0.245*wHeight, false, cible1],
        [0.411*wWidth, 0.7285*wHeight, false, cible3],
        [0.516*wWidth, 0.7285*wHeight, false, cible4],
        [0.14*wWidth, 0.7295*wHeight, false, cible2],
        [0.244*wWidth, 0.7295*wHeight, false, cible2],
        [0.669*wWidth, 0.7295*wHeight, false, cible2],
        [0.772*wWidth, 0.7295*wHeight, false, cible2]]; // [x, y, visibilité, dictionnaire]

    // --- LISTENERS --- //
    monCanvas.addEventListener("click", clicCanvas, false);

    if (window.DeviceOrientationEvent) {
        window.addEventListener("deviceorientation", orientation, false);
    }

    /*monCanvas.addEventListener('mousemove', function (evt) {
        var mousePos = getMousePos(monCanvas, evt);
        targetX = mousePos.x - targetWidth / 2;
        targetY = mousePos.y - targetHeight / 2;

        makeTarget();
    }, false);*/

// ----------------------------------------- Programme -------------------------------------------------------------- //

    preLoad();

};

