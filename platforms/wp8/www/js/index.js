/**
 * Created by Lucas on 03/04/2016.
 */



function launch(game) {
    var pseudo = document.getElementById('pseudo').value;
    console.log(pseudo);

    if (pseudo == '' && game!='Pong') {
        console.log('null');
        document.getElementById("pseudo").className = "pseudo formInvalid";
        setTimeout(function reload() {
            document.getElementById("pseudo").className = "pseudo";
        }, 1500);
    }

    else {
        window.open(game + '/index.html?pseudo='+pseudo);

    }

}