/**
 * Created by Lucas on 14/01/2016.
 */
//        var canvasWidth = window.innerWidth;
//        var canvasHeight = window.innerHeight;
var shipSize = window.innerWidth / 5;
var shipX = (window.innerWidth / 2);
var shipY = (window.innerHeight / 2);
var canvas;
var canvas2;
var shipsrc = 'img/spaceships/ship.png';
var ctx;
var ctx2;
var ship = new Image();
var gyro = true;
var touch = false;
var gameover = false;
var pause = false;
var start = true;
var y = 0; //scrolling wallpaper

//----- SCORE+HIGHSCORE -----//
var score = 0;
var inter;
Url = {
    get get() {
        var vars = {};
        if (window.location.search.length !== 0)
            window.location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
                key = decodeURIComponent(key);
                if (typeof vars[key] === "undefined") {
                    vars[key] = decodeURIComponent(value);
                }
                else {
                    vars[key] = [].concat(vars[key], decodeURIComponent(value));
                }
            });
        return vars;
    }
};
var username = Url.get.pseudo;

//----- ASTEROID+LASER -----//

var aste = new Image();
var explosionsnd = new Audio('audio/vaisseau_explosion.mp3');
var tiresnd = new Audio('audio/fire.mp3');
var collisionsnd = new Audio('audio/collision.mp3');
var astesize = window.innerWidth / 6;
var spawnLineY = -100;
var coef = 1;
var spawnRate = 750;
var spawnRateOfDescent = 3;
var lastSpawn = -1;
var objects = [];
var laser = [];
var req;
var req2;
var ctx3;

//------ CANVAS BLUR -------//
var ctx4;


// -----------  Fonction principale ----------- \\

function canvasSpaceGame() {

    resize();

    //console.log(window.username);

    canvas = document.getElementById('monCanvas');

    window.addEventListener('keydown', down, true);

    window.addEventListener('resize', resize, false);

    //window.addEventListener("load", function () {
    //    if (!window.pageYOffset) {
    //        hideAddressBar();
    //    }
    //});

    if (window.DeviceOrientationEvent) {
        window.addEventListener("deviceorientation", orientation, false);

    }

    canvas.addEventListener('mousemove', function (e) {
        if (touch) {
            var mousePos = getMousePos(canvas, e);
            shipX = mousePos.x - shipSize / 2;
            shipY = mousePos.y - shipSize / 2;

            //makeShip();

//				console.log($(window).width()+'x'+
        }
    }, false);

    canvas.addEventListener('touchstart', function (e) {
        if (touch) {
            touchobj = e.changedTouches[0];
            canvasleft = parseInt(canvas.style.left);
            startx = parseInt(touchobj.clientX);
            starty = parseInt(touchobj.clientY);
            e.preventDefault();
//                console.log('touche en : ('+startx + ', '+starty+')');
//            shoot();
        }
    }, false);

    canvas.addEventListener('touchmove', function (e) {
        if (touch) {
            var marge = window.innerWidth * 0.05;
            touchobj = e.changedTouches[0];
            var distX = parseInt(touchobj.clientX - shipSize / 1.9);
            var distY = parseInt(touchobj.clientY - shipSize / 1.5);
            //if (!pause) {
            if (distX > shipX - marge && distX < shipX + marge && distY > shipY - marge && distY < shipY + marge) {
                shipX = distX;
                shipY = distY;
                //console.log('distX :' + distX + ', distY :' + distY + ', shipX :' + shipX + ', shipY :' + shipY);
//				 console.log(window.innerWidth+' ,'+window.innerHeight);
//               console.log(canvas.width + ' ,' + canvas.height);
//                makeShip();
                e.preventDefault();
            }
            if (shipX > canvas.width - shipSize - 2) {
                shipX = canvas.width - shipSize - 2;
            }
            else if (shipX < 0) {
                shipX = 0;
            }
            if (shipY > canvas.height - shipSize - 2) {
                shipY = canvas.height - shipSize - 2;
            }
            else if (shipY < 0) {
                shipY = 0;
            }
            //}
        }
    }, false);

    if (!start) {
        animate();
        //StartMove();
    }

    if (inter) {
        clearInterval(inter);
    }
    inter = setInterval(interval, 16);

}


// ----------- Fonctions appelées ------------ \\

function interval() {
    if (!start) {
        scoreInc();
    }
    makeShip();
    //if (!gameover && !pause && !start) {
    //    y += 0.4;
    //    $('body').css('background-position', '0 ' + y + 'px');
    //}
}


function startScreen() {
    start = false;
    canvasSpaceGame();
}


function makeShip() {
    if (!gameover && !pause) {
        canvas = document.getElementById('monCanvas');
        if (canvas.getContext) {
            ctx = canvas.getContext('2d');
            ship.src = shipsrc;
            ship.onload = function () {
                ctx.clearRect(0, 0, canvas.width * 2, canvas.height * 2);
                ctx.drawImage(ship, shipX, shipY, shipSize, shipSize);
            }
        }
    }
    else {

    }
}


function down(e) {
    if (!gameover && !pause && !start) {
        //fleche droite
        if (e.keyCode === 39) {
            if (shipX < canvas.width - shipSize) {
                shipX += 50;
                //makeShip();
            }
        }

        //fleche gauche
        else if (e.keyCode === 37) {
            if (shipX > 0) {
                shipX -= 50;
                //makeShip();
            }
        }

        //fleche du bas
        else if (e.keyCode === 40) {
            if (shipY < canvas.height - shipSize) {
                shipY += 50;
                //makeShip();
            }
        }

        //fleche haut
        else if (e.keyCode === 38) {
            if (shipY > 0) {
                shipY -= 50;
                //makeShip();
            }
        }

        else if (e.keyCode === 32) {
            shoot();
            gaugeAdjust();

        }
    }
}


function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}


function orientation(event) {
//            console.log("Magnetometer: " + event.beta + ", " + event.gamma);
    if (gyro == true && !pause && !start) {
        if (event.gamma > 0 && shipX < canvas.width - shipSize) {
            shipX += event.gamma;
        }


        else if (event.gamma < 0 && shipX > 0) {
            shipX += event.gamma;
        }


        if (event.beta < 0 && shipY > 0) {
            shipY += event.beta;
        }

        else if (event.beta > 0 && shipY < canvas.height - shipSize) {
            shipY += event.beta;
        }

        //console.log("X,Y: " + shipX + ", " + shipY);

        //makeShip();
    }
}


function resize() {
    canvas = document.getElementById("monCanvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    shipX = (canvas.width / 2) - (shipSize / 2);
    shipY = (canvas.height / 2) - (shipSize / 2);
    //makeShip();
    //console.log(canvas.width + ' ,' + canvas.height);

}


function gyroSwitch() {
    gyro = gyro != true;
    //console.log(gyro);
}


function touchSwitch() {
    touch = touch != true;
    //console.log(touch);
}


function spawnRandomObject() {
    //var choose = Math.random();
    //var skin;
    //if (choose >= 0 && choose < 0.33) {
    //    skin = 'img/asteroid1.jpg';
    //}
    //if (choose > 0.33 && choose <= 0.66) {
    //    skin = 'img/asteroid2.jpg';
    //}
    //else {
    //    skin = 'img/asteroid3.jpg';
    //}

    var object = {
        x: Math.random() * (canvas.width - 30) + 15,
        y: spawnLineY
        //skin: skin
    };

    objects.push(object);
    console.log(objects.length);
}


function animate() {
    if (!gameover) {
        canvas2 = document.getElementById('monCanvas');
        if (canvas2.getContext) {
            ctx2 = canvas2.getContext('2d');

            var time = Date.now();

            if (time > (lastSpawn + spawnRate / coef) && !pause) {
                lastSpawn = time;
                spawnRandomObject();
            }
            req = requestAnimationFrame(animate);
            aste.src = 'img/asteroid1.jpg';
            for (var i = 0; i < objects.length; i++) {

                var object = objects[i];
                //aste.src = object.skin;

                if (object.y > canvas.height + 100) {
                    objects.splice(i, 1);
                }
                else {
                    if (!pause) {
                        object.y += spawnRateOfDescent * coef;
                        //console.log(object.y)
                        if (shipY < object.y + astesize / 2 && shipY > object.y && shipX < object.x + astesize / 2 && shipX + shipSize / 2 > object.x) {
                            gameOver();
                        }

                        if (laser.length > 0) {
                            for (var j = 0; j < laser.length; j++) {

                                var tire = laser[j];
                                //console.log("done", "length=" + laser.length, 'j=' + j, "tire=" + tire);

                                if (tire.y - 50 < object.y + astesize / 3 && tire.y - 50 > object.y && tire.x + shipSize / 2 > object.x && tire.x + shipSize / 2 < object.x + astesize) {
                                    laser.splice(j, 1);
                                    objects.splice(i, 1);
                                    tiresnd.load();
                                    collisionsnd.play();
                                    //console.log("collision");
                                    return;
                                }

                            }
                        }

                        ctx2.drawImage(aste, object.x, object.y, astesize, astesize);
                    }
                }
            }
        }
    }
}


function setSpeed(value) {
    coef = value;
    console.log("coef=" + coef);
    //spawnRandomObject();
    //animate();
    //canvasSpaceGame();
}


function setSize(value) {
    astesize = window.innerWidth / value;
}


function restart() {
    gameover = false;
    //gameoverloc=false;
    pause = false;
    score = 0;
    heightGauge = 40;
    objects = [];
    shipX = (canvas.width / 2) - (shipSize / 2);
    shipY = (canvas.height / 2) - (shipSize / 2);
    //makeShip();
    cancelAnimationFrame(req);
    //requestAnimationFrame(animate);
    //canvasSpaceGame();
    animate();
}


function pausebtn() {
    pause = !pause;
    if (pause && !gameover) {
        ctx4 = canvas.getContext('2d');
        ctx4.beginPath();
        ctx4.globalAlpha = 0.8;
        ctx4.fillRect(0, 0, canvas.width, canvas.height);
    }

    else {
        if (!gameover) {
            ctx4.globalAlpha = 1;
            ctx4.clearRect(0, 0, canvas.width, canvas.height);
            //console.log('clear rect');
        }
    }

}


function gameOver() {
    explosionsnd.play();
    //console.log("Game Over");
    //cancelAnimationFrame(req);
    gameover = true;
    var userdata = {};
    var bestScore = 0;
    var bestPlayer;
    var find = false;
    //localStorage["HighScores"] = [];
    if (localStorage["HighScoresSpace"] == undefined) {
        localStorage["HighScoresSpace"] = [];
        var localstore = [];
        //console.log("done");
    }

    else {
        var localstore = JSON.parse(localStorage["HighScoresSpace"]);
    }

    //console.log("premiere recup du localstorage :", localstore);
    //var localstore = [];

    if (typeof localstore === typeof []) {
        for (var i = 0; i < localstore.length; i++) {
            if (localstore[i].user == username) {
                find = true;
                userdata = localstore[i];
                localstore.splice(i, 1);
                //console.log("User trouvé, enlevé de localstore :", localstore);
            }
        }

        if (find) {
            if (userdata.score < score) {
                userdata.score = JSON.stringify(score);
                //console.log("Nouveau score supperieur à celui stocké :", score, ">", userdata.score);
            }
        }

        else if (!find) {
            userdata = {user: username, score: JSON.stringify(score)};
            //console.log("creation de l'objet car nouveau user");
        }
    }

    else {
        localstore = [];
        userdata = {user: username, score: JSON.stringify(score)};
        //console.log("creation de l'objet car nouveau user ET premiere ecriture dans tableau");
    }

    //console.log("userdata post verif :", userdata);
    localstore.push(userdata);

    for (var i = 0; i < localstore.length; i++) {
        //console.log(localstore[i].score, "bestscore:" + bestScore);
        if (localstore[i].score > bestScore) {
            //console.log("nouveau meilleur", localstore[i].score + " > " + bestScore);
            bestScore = localstore[i].score;
            bestPlayer = localstore[i].user;
        }
    }
    //console.log("localstore avec userdata push inside :", localstore);
    localStorage["HighScoresSpace"] = JSON.stringify(localstore);

    //console.log("localStorage final de HighScores :", localStorage["HighScoresSpace"]);


    document.getElementById("scoredisplay").innerHTML = "";
    document.getElementById("txthighscore").innerHTML = "Score : " + score + " | Highscore de " + username + " : " + userdata.score + "<br/>Meilleur joueur " + bestPlayer + " avec " + bestScore + " points";
    onGameOver();
//console.log("Highscore=" + highscore);
}


function scoreInc() {
    if (!pause && !gameover) {
        score += 1;
        document.getElementById("scoredisplay").innerHTML = score;
    }
}


function shoot() {
    if (!pause && !gameover && heightGauge >= 25) {
        tiresnd.load();
        tiresnd.play();
        var tire = {
            x: shipX,
            y: shipY
        };
        laser.push(tire);
        cancelAnimationFrame(req2);
        drawLine();
        ctx3.strokeStyle = 'purple';
        ctx3.lineWidth = 4;
        ctx3.shadowBlur = 60;
        ctx3.shadowColor = "purple";
    }


}


function drawLine() {
    var margeX = window.innerWidth / 20;
    var margeY = window.innerHeight / 10;
    canvas = document.getElementById('monCanvas');
    if (canvas.getContext) {
        ctx3 = canvas.getContext('2d');
        //ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx3.beginPath();
        req2 = requestAnimationFrame(drawLine);
        for (var i = 0; i < laser.length; i++) {

            var tire = laser[i];

            if (tire.y < 0) {
                laser.splice(i, 1);
            }

            else {
                if (!pause) {
                    tire.y -= 20;
                }

                ctx3.moveTo(tire.x + margeX, tire.y + margeY);
                ctx3.lineTo(tire.x + margeX, tire.y);
                ctx3.moveTo(tire.x + margeX * 3.1, tire.y + margeY);
                ctx3.lineTo(tire.x + margeX * 3.1, tire.y);
                ctx3.stroke();

            }

        }

    }
    ctx3.shadowBlur = 0;
    //makeShip();

}


function selectShip(ship) {
    shipsrc = ship;
    $('#myModal').modal('hide');
    if (pauseloc) {
        pauseloc = false;
        $("#txtpause").fadeToggle("fast");
        pausebtn()
    }
}